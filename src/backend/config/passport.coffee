# To query for a user with given username
mongoose = require 'mongoose'
# Entrypoint for query
User = require '../model/User'
# Passport local authentication strategy
LocalStrategy = (require 'passport-local').Strategy

module.exports = (passport) ->
	passport.use new LocalStrategy (username, password, done) ->
		console.log('DEBUG : checking credentials for ' +
			username + '/@' + password)
		# Querying for user
		User.findOne {username: username}, (err, user) -> 
			# Any errors -> we are done with an error
			if err is on then done err 
			# No user with such a username
			if !user
				# We are done with 'false' and no such user message
				console.log 'No such user'
				return done null, false, 'No such user' 

			# If password doesn't match
			if password != user.password
				return done null, false, 'Incorrect password'
			# Authentication OK
			else done null, user

	# Now we need to store user credentials in session
	# We need to supply serializeUser and deserializeUser to Passport

	passport.serializeUser (user, done) ->
		done null, {id: user._id}

	passport.deserializeUser (id, done) ->
		User.findById id.id, (err, user) ->
			done err, user
