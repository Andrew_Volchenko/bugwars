# For path manipulation
path = require 'path' 

# To start new express app
express = require 'express'	
# We use Mongo session store to persist sessions
MongoStore = (require 'connect-mongo') express 

module.exports = (passport) ->
# Starting new express application
	app = express()	
	# Establishing root dir to resolve all routes against
	# It is two hops up. We are in backend/config
	# Server is at the same level as backend folder
	# ../ -> backend/
	# ../../ -> path with server.js
	root = path.normalize(path.join(__dirname, '../../')) 
	
	# the first one is said to be used by hosters
	app.set 'port', process.env.PORT ? 3000 
	# Folder with views
	app.set 'views', path.join(root, 'backend/views')

	console.log('DEBUG: path with views' + path.join(root, 'backend/views'))

	# Template engine used
	app.set 'view engine', 'jade'

	app.use express.logger 'dev'

	sessionSecret = 'jnfjkj3hri3hr03h30h03hgbu3i0h10jabfqp[fa3qqrqr'

	# Kind of express filters

	# Express middleware to parse request body and attach it to req.body
	app.use express.bodyParser()
	# Express middleware to support HTTP method overriding
	# Adding explicit cookie info to request object
	cookieParser = express.cookieParser(sessionSecret)
	app.use cookieParser

	# Establishing mongo session store
	# All sessions are persistent

	# Session secret and session store
	# also need to get passed to socketio.passport

	sessionStore = new MongoStore
			url: 'mongodb://localhost:27017/bugwars'
			collection: 'sessions'

	app.use express.session {
		secret: sessionSecret
		store: sessionStore
	}

	# Passport middleware

	app.use passport.initialize()
	app.use passport.session()

	# Express middleware to serve static context
	console.log path.join(root, '/static')
	app.use '/static', express.static(path.join(root, '/static'))

	app.use app.router

	console.log 'CookieParser: '
	console.log cookieParser

	{
		app: app
		sessionStore: sessionStore
		sessionSecret: sessionSecret
		cookieParser: cookieParser
	}