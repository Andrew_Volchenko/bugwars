module.exports = (io, socket) ->

	socket.on 'create or join', (game) ->
		numberOfPlayers = io.sockets.in(game).length

		if numberOfPlayers == 0 # We created new game
			socket.join game
			socket.emit 'create', game
		else if numberOfPlayers == 1
			io.sockets.in(game).emit 'join', game
			socket.join game

	socket.on 'initializeRTCAndConfirm', (game) ->
		io.sockets.in(game).emit 'initializeRTCAndConfirm', true

	socket.on 'confirmingRTCInitialized', (game) ->
		io.sockets.in(game).emit 'confirmingRTCInitialized', true

	socket.on 'offer', (wrappedDescription) ->
		io.sockets.in(wrappedDescription.game) 'offer', 
		wrappedDescription.description

	socket.on 'answer', (wrappedDescription) ->
		io.sockets.in(wrappedDescription.game) 'answer', 
		wrappedDescription.description
