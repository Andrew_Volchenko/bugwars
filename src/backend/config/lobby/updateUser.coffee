log = require '../../util/log'
userUpdater = require '../../model/userUpdater'

module.exports = (io, socket) ->
	emit = (type, message) ->
		io.sockets.emit type, message

	updateUserInfo = (data) ->
		userUpdater socket.handshake.user, data, ->
			socket.emit 'updatedUserInfo', true

	socket.on 'updateUserInfo', (data) ->
		log 'User asked via socket to update her info'
		updateUserInfo data