userDatabase = require '../../model/User'

log = (msg) ->
	console.log ('DEBUGGING: ' + msg)

module.exports = (io, socket) ->
	emit = (type, message) ->
		userDatabase
		io.sockets.emit type, message

	map = ->
		emit @username, (@gamesWon / (@gamesWon + @gamesLost)) 
	reduce = (key, values) ->
		# MapReduce is bypassing the reduce stage if there's only one result
		# but if theres more, it's an error
		throw new Error 'Duplicate users in the DB!'
	finalize = (key, value) ->
		username: key
		value: value

	mapReduce =
		map: map
		reduce: reduce
		finalize: finalize

	postProcessor = (err, results) ->
		if err
			throw err
		results = results.map (result) ->
			username: result.value.username
			winrate: (result.value.value or 0)
		results.sort (result1, result2) ->
			result1.winrate < result2.winrate
		socket.emit 'statistics', results.slice 0, 5


	sendStatistics = ->
		log 'Aggregating statistics'
		userDatabase.mapReduce mapReduce, postProcessor

	# Initial push request
	sendStatistics()