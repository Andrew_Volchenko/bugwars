log = (msg) ->
	console.log ('DEBUGGING: ' + msg)

wroteLongMessage = (message) ->
	'User `' + message.username + '` wrote a ' +
			'long message'

wroteMessage = (message) ->
	'User `' + message.username + '` wrote a ' +
			'message: ' + message.message

userSaid = (message) ->
	'User `' + message.username + '` said : ' +
		message.message

userTyping = (message) ->
	'User `' + message.username + '` is typing ' +
		'a message'

userErasing = (message) ->
	'User `' + message.username + '` is erasing ' +
		'her message'

userIdleWith = (message) ->
	'User `' + message.username + '` has dropped pen'

YOUR_MESSAGE_IS_TOO_LONG = 'Your message is too long'
MAX_ALLOWED_MESSAGE_LENGTH = 400

SERVER_MESSAGE_ID = 'serverMessage'
NEW_MESSAGE_ID = 'newMessage'
SEND_MESSAGE_ID = 'sendMessage'
TYPING_MESSAGE_ID = 'typing'
ERASING_MESSAGE_ID = 'erasing'
IDLE_MESSAGE_ID = 'idle'
IDLE_CONFIRM_MESSAGE_ID = 'idleConfirm'

SERVER = 'SERVER'

module.exports = (io, socket) ->
	emit = (type, message) ->
		io.sockets.emit type, message

	handleNewMessage = (msg) ->
		if msg.length > MAX_ALLOWED_MESSAGE_LENGTH
			log wroteLongMessage msg
			socket.emit SERVER_MESSAGE_ID, 
				username: SERVER
				msg: YOUR_MESSAGE_IS_TOO_LONG
		else
			log wroteMessage msg
			emit NEW_MESSAGE_ID, message msg

	username =  -> socket.handshake.user.username

	message = (msg) ->
		username: username()
		msg: msg

	socket.on SEND_MESSAGE_ID, (message) ->
		handleNewMessage message

	socket.on TYPING_MESSAGE_ID, ->
		log userTyping username()
		emit TYPING_MESSAGE_ID, username()

	socket.on ERASING_MESSAGE_ID,  ->
		log userErasing username()
		emit ERASING_MESSAGE_ID, username()

	socket.on IDLE_MESSAGE_ID,  ->
		log userIdleWith username()
		emit IDLE_MESSAGE_ID, username()
