log = (msg) ->
	console.log ('DEBUGGING: ' + msg)

module.exports = (io, socket) ->
	emit = (type, message) ->
		io.sockets.emit type, message

	sendUserData = ->
		user = socket.handshake.user
		winrate = user.gamesWon / (user.gamesWon + user.gamesLost)
		socket.emit 'yourUserInfo', 
			username: user.username
			winrate: winrate
			fastGame: user.fastGame
			longGame: user.longGame

	socket.on 'myUserInfo', ->
		log 'Asking for user info'
		sendUserData()

	# Initial push request
	sendUserData()