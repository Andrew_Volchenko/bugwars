hash = (require 'crypto').createHash

log = (msg) ->
	console.log ('DEBUGGING: ' + msg)

# Game is characterised by
# 1. Hash (id)
# 2. Number of players (needs to be updated on when
#	someone joins the game)
# 3. Who started it (needs to be updated once on when 
#	the game is created)
# 4. How long it/ lasts (tricky). In msecs.

games = []

HASH_ALGORITHM = 'md5'

module.exports = (io, socket) ->
	emit = (type, message) ->
		io.sockets.emit type, message

	currentGames = ->
		log 'Sending current games to socket'
		socket.emit 'currentGames', games

	createGameHash = (initiator) ->
		currentDate = new Date().getTime
		stringToDigest = initiator + currentDate
		newHash = hash HASH_ALGORITHM
		newHash.update stringToDigest
		newHash.digest 'hex'

	socket.on 'selfGameIdentifier', ->
		username = socket.handshake.user.username
		socket.emit 'selfGameIdentifier', 
			username: username
			games: games

	socket.on 'newGame', (data) ->
		initiator = socket.handshake.user.username
		hashString = createGameHash initiator
		numberOfPlayers = data.numberOfPlayers
		games.push
			hash: hashString
			initiator: initiator
			numberOfPlayers: numberOfPlayers
		log 'Broadcasting current games to all sockets, as a ' +
		'new game was created'
		emit 'currentGames', games 
	# Initial push request
	log 'Sending initial current games to socket'
	log games
	currentGames()