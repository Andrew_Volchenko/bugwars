socketHook = require('socket.io').listen

chat = require './lobby/chat'
self = require './lobby/userInfo'
games = require './lobby/games'
statistics = require './lobby/statistics'
updateUser = require './lobby/updateUser'

signalingServer = require './game/signalingServer'

module.exports = (appObj) ->
	sessionStore = appObj.sessionStore
	sessionSecret = appObj.sessionSecret
	app = appObj.app

	io = socketHook app.listen((app.get 'port'))

	handleLobbySocket = (socket) ->
		chat io, socket
		self io, socket
		games io, socket
		statistics io, socket
		updateUser io, socket
	
	handleGameSocket = (socket) ->
		signalingServer io, socket

	# When there is new socket
	io.of('/lobby').on 'connection', handleLobbySocket
	io.of('/game').on 'connection', handleGameSocket

	(require './socketio-passport')(io, appObj)

	console.log 'SOCKET.IO CONFIGURATION INITIALIZED'