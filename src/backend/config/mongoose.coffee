join = (require 'path').join
extname = (require 'path').extname
fs = require 'fs'

mongoose = require 'mongoose'

log = console.log

module.exports = ->
	hop = '../'
	models = join __dirname, hop, '/model' 
	# establishing connection to mongo
	mongoose.connect 'mongodb://localhost:27017/bugwars' 

	# path where models are stored
	log('DEBUG: model path : ' + models)

	# this is how we pre-load models
	fs.readdirSync(models).forEach (file) ->
		log('DEBUG: model filename : ' + file)
		if (extname file) is '.js'
			log('DEBUG: requiring model : ' + join(models, file))
			require join(models, file)