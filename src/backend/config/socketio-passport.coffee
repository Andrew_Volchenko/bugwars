passportSocketIo = require('passport.socketio')
express = require('express')

module.exports = (io, appObj) ->
	fail = (data, message, error, accept) ->
		if error
			throw new Error message
		else
			console.log message
			accept null, false
	console.log appObj.cookieParser

	success = (data, accept) ->
		accept null, true

	appObj.cookieParser.fakeField = 'fakeField0'
	io.set 'authorization', passportSocketIo.authorize
		cookieParser: express.cookieParser,
		secret: appObj.sessionSecret
		store: appObj.sessionStore
		success: success
		fail: fail
		