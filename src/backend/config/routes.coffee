http = require 'http'

User = require '../model/User'

module.exports = (app, passport) ->

	# Application entrypoint
	app.get '/', (request, response) ->
		if request.isAuthenticated()
			response.render 'lobby', {user: request.user}
		else
			response.render 'login'

	# Login view
	app.get '/login', (request, response) ->
		response.render 'login'

	# Authentication
	app.post '/login', (request, response, next) ->
		console.log 'DEBUG: Someone`s trying to login..'
		console.log request.user
		next()

	app.post '/login', passport.authenticate 'local', {
		successRedirect: '/',
		failureRedirect: '/login' 
	}

	# Logout
	app.get '/logout', (request, response) ->
		request.logout()
		response.redirect '/login'

	# Going to game
	app.get '/game', (request, response) ->
		response.header "Content-Type", "text/html"
		response.render 'game'

	app.post '/signup', (request, response) ->
		mail = request.body.mail
		name = request.body.name
		surname = request.body.surname
		username = request.body.username
		password = request.body.password

		# Static async SS validation
		(require './login/registrationValidator') request.body, (valid, invalid) ->
			if valid
				User.create
					mail: mail
					name: name
					surname: surname
					username: username
					password: password
				response.send 200
			else
				response.send 409, invalid

	app.get '/userExists/:possibleUsername', (request, response) ->
		(require './login/usernameExistsChecker') request.params.possibleUsername, 
		(err, result) ->
			if err then throw err
			if result
				response.send exists: true
			else
				response.send exists: false

	# Updating user from http
	app.post '/updateUser', (request, response) ->
		(require '../model/userUpdater') request.user, 
			won: request.body.won
			lost: request.body.lost
			time: request.body.time, (err) ->
				if err then throw err
				response.send 200

	# We configured all routes, we kick off the server
	http.createServer app
