log = (msg) ->
	console.log('DEBUGGING: ' + msg)

invalidMail = (mail) ->
	(mail.indexOf '@') == -1

invalidUsername = (username) ->
	username.length < 6

invalidPassword = (password) ->
	password.length < 6

module.exports = (body, callback) ->
	log 'Starting server-side validation of sign-up information'
	username = body.username
	(require './usernameExistsChecker') username, (err, result) ->
		if err then throw err
		if result or invalidUsername username # Still could be invalid
			callback false, invalid: ['username']
			log('Username `' + username + '` is invalid')
			return
		else if !body.name
			callback false, invalid: ['name']
			log('Name `' + body.name + '` is invalid')
			return
		else if !body.surname
			callback false, invalid: ['surname']
			log('Surname `' + body.surname + '` is invalid')
			return
		else if invalidMail body.mail
			callback false, invalid: ['mail']
			log('Mail `' + body.mail + '` is invalid')
			return
		else if invalidPassword body.password
			callback false, invalid: ['password']
			log('Password `' + body.password + '` is invalid')
			return
		else
			log('Username `' + username + '` is valid')
			log('Name `' + body.name + '` is valid')
			log('Surname `' + body.surname + '` is valid')
			log('Mail `' + body.mail + '` is valid')
			log('Password `' + body.password + '` is valid')
			callback true
