User = require '../../model/User'

module.exports = (possibleUsername, callback) ->
		User.findOne {username: possibleUsername}, (err, result) ->
			if err
				callback err, false
			if result
				callback null, true
			else
				callback null, false