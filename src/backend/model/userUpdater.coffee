log = (msg) ->
	console.log('DEBUGGING: ' + msg)

User = require './User'

module.exports = (user, newerInfo, callback) ->
	log('Starting updating user `' + user.username + '`')
	log('Newer info is : ' + (JSON.stringify newerInfo))
	if newerInfo.won
		log 'Updating the games she won'
		user.statistics.gamesWon++
	if newerInfo.lost
		log 'Updating the games she lost'
		user.statistics.gamesLost++
	if newerInfo.time > user.statistics.longestGame
		log 'Updating her longest game'
		user.statistics.longestGame = newerInfo.time
	if newerInfo.time < user.statistics.fastestGame
		log 'Updating her fastest game'
		user.statistics.fastestGame = newerInfo.time
	User.findByIdAndUpdate user._id, 
		statistics:
			gamesWon: user.statistics.gamesWon
			gamesLost: user.statistics.gamesLost
			longestGame: user.statistics.longestGame
			fastestGame: user.statistics.fastestGame
		(err, foundObject) ->
			if err
				log('Error while executing mongoose`s FBIAU:' + err)
				throw err
			callback()