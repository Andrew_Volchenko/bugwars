mongoose = require 'mongoose'
Schema = mongoose.Schema

UserSchema = new Schema
	username: String
	email: String
	password: String
	avatar: String
	statistics:
		gamesWon: Number
		gamesLost: Number
		longestGame: Number
		fastestGame: Number

console.log ('DEBUG: About to initialize UserSchema')

module.exports = mongoose.model 'User', UserSchema, 'Users'