var connection = new Mongo(),
	database = connection.getDB('bugwars');

if (!database.Users.exists()) {
	database.Users.insert({
		username: 'admin', 
		password: 'admin', 
		statistics: {
			gamesWon: 2, 
			gamesLost: 4,
			longestGame: 5,
			fastestGame: 2
		}});

	database.Users.insert({
		username: 'sampleUser1', 
		password: 'sampleUser1', 
		statistics: {
			gamesWon: 35, 
			gamesLost: 41,
			longestGame: 15,
			fastestGame: 32
		}});

	database.Users.insert({
		username: 'sampleUser2', 
		password: 'sampleUser2', 
		statistics: {
			gamesWon: 167, 
			gamesLost: 513,
			longestGame: 188,
			fastestGame: 51
		}});

	database.Users.insert({
		username: 'sampleUser3', 
		password: 'sampleUser3',
		statistics: {
			gamesWon: 71, 
			gamesLost: 72,
			longestGame: 2,
			fastestGame: 1
		}});
}