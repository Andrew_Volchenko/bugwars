# to configure auth
passport = require 'passport'

process.bugWarsDebugSettings = true

# we configure mongoose
(require './backend/config/mongoose')()

# we configure passport
(require './backend/config/passport') passport

# we configure passport
app = (require './backend/config/express') passport 

# we configure express routes
(require './backend/config/routes') app.app, passport 

# we configure sockets
(require './backend/config/sockets') app

exports = app.app

console.log 'Server is listening on ' + (app.app.get 'port')