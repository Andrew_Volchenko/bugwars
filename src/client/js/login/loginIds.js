var loginIds = {
	registration: {
		wrapper: 'signup',
		formId: 'signupBox',
		submitId: 'signupSubmit',
		successComponentId: 'signupSuccess',

		formElementIds: {
			mail: 'signupMail',
			username: 'signupUsername',
			name: 'signupName',
			surname: 'signupSurname',
			password: 'signupPassword'
		},

		validationElementIds: {
			mail: 'signupMailValidation',
			username: 'signupUsernameValidation',
			name: 'signupNameValidation',
			surname: 'signupSurnameValidation',
			password: 'signupPasswordValidation'
		}	
	}
};