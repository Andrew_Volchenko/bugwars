function RegistrationForm (window, ids) {
	var document = window.document,
		form = document.getElementById(ids.formId),
		submitButton = document.getElementById(ids.submitId),
		successComponent = document.getElementById(ids.successComponentId);

	var mail = document.getElementById(ids.formElementIds.mail),
		username = document.getElementById(ids.formElementIds.username),
		name = document.getElementById(ids.formElementIds.name),
		surname = document.getElementById(ids.formElementIds.surname),
		password = document.getElementById(ids.formElementIds.password);

	var mailValidation = document.getElementById(ids.validationElementIds.mail),
		usernameValidation = document.getElementById(ids.validationElementIds.username),
		nameValidation = document.getElementById(ids.validationElementIds.name),
		surnameValidation = document.getElementById(ids.validationElementIds.surname),
		passwordValidation = document.getElementById(ids.validationElementIds.password);

	var valid = [true, true, true, true, true];
	var fields = [mail, name, surname, username, password];
	var validationFields = [mailValidation, nameValidation, surnameValidation, usernameValidation, passwordValidation];
	
	var i;

	for (i = 0; i < validationFields.length; i++) {
		hide(validationFields[i]);
	}

	hide(successComponent);

	function stop(evt) {
		evt.preventDefault();
	}

	function show(field) {
		field.style.display = 'block';
	}

	function hide(field) {
		field.style.display = 'none';
	}

	function highlight(field) {
		field.style.background = 'crimson';
	}

	function unHighlight(field) {
		field.style.background = 'white';
	}

	submitButton.addEventListener('click', function (evt) {

		for (var i = 0; i < fields.length; i++) {
			if (!valid[i] || fields[i].value === '') {
				highlight(fields[i]);
				show(validationFields[i]);
				stop(evt);
				return false;
			} else {
				unHighlight(fields[i]);
			}
		}

		// Everything is valid

		var formData = new FormData(form);

		var xhr = new XMLHttpRequest();

		xhr.open('POST', form.getAttribute('action'));

		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				if (xhr.status === 200) {
					hide(form);
					show(successComponent);
				} else if (xhr.status === 409) {
					var response = JSON.parse(xhr.responseText);
					for (var i = 0; i < response.invalid.length; i++) {
						highlight(fields[fields.indexOf(response.invalid[i])]);
						show(fields[fields.indexOf(response.invalid[i])]);
					}
				}
			}
		};

		xhr.send(formData);
		stop(evt);
	});

	// Validators

	function usernameExists(username, cb) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4 && xhr.status === 200) {
				cb(JSON.parse(xhr.responseText).exists);
			}
		};

		xhr.open('GET', '/userExists/' + username);
		xhr.send();
	}

	mail.addEventListener('input', function () {
		if (mail.value.indexOf('@') === -1) {
			// Something very simple, it is invalid
			valid[fields.indexOf(mail)] = false;
			highlight(mail);
		} else {
			valid[fields.indexOf(mail)] = true;
			unHighlight(mail);
		}
	});

	username.addEventListener('input', function () {
		if (username.value === '') {
			valid[fields.indexOf(username)] = false;
			highlight(username);
			return;
		}

		usernameExists(username.value, function (exists) {
			if (username.value.length < 6 || exists) {
				// It is invalid
				// Also check for it already exists
				valid[fields.indexOf(username)] = false;
				highlight(username);
			} else {
				valid[fields.indexOf(username)] = true;
				unHighlight(username);
			}
		});
	});

	password.addEventListener('input', function () {
		if (password.value.length < 6) {
			// It is invalid
			valid[fields.indexOf(password)] = false;
			highlight(password);
		} else {
			valid[fields.indexOf(password)] = true;
			unHighlight(password);
		}
	});

	function defaultFieldValidator(field) {
		return function () {
			if (field.value === '') {
				valid[fields.indexOf(field)] = false;
				highlight(field);
			} else {
				valid[fields.indexOf(field)] = true;
				unHighlight(field);
			}
		};
	}

	name.addEventListener('input', defaultFieldValidator(name));
	surname.addEventListener('input', defaultFieldValidator(surname));
}