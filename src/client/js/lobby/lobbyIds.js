var lobbyIds = {
	userInfo: {
		id: 'userInfo',
		children: {
			login: 'login',
			games: 'games',
			fastGame: 'fastGame',
			longGame: 'longGame'
		}
	},

	statistics: {
		id: 'statistics',
		children: {
			list: 'ulStatistics'
		}
	},

	chat: {
		id: 'chat',
		children: {
			messages: 'chatMessages',
			input: 'chatInput'
		}
	},

	games: {
		id: 'hostedGames',
		children: {
			list: 'gamesList'
		}
	},

	newGame: {
		id: 'newGame',
		children: {
			createGameButton: 'createGameButton',
			newGameDialog: 'newGameDialog',
			closeNewGameDialogButton: 'closeNewGameDialogButton',
			numberOfPlayers: 'numberOfPlayers',
			startGameButton: 'startGameButton'
		}
	}

};