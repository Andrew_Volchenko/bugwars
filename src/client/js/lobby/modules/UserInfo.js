function UserInfo(window, socket, userInfoIds) {
	var document = window.document,
		username = document.getElementById(userInfoIds.children.login),
		games = document.getElementById(userInfoIds.children.games),
		longGame = document.getElementById(userInfoIds.children.longGame),
		fastGame = document.getElementById(userInfoIds.children.fastGame);

	function setText(node, textString) {
		var text = node.childNodes[0];

		if (!text) {
			text = document.createTextNode(textString);
			node.appendChild(text);
			return;
		}
		text.replaceData(0, text.length, textString);
	}

	function updateFields (user) {
		setText(username, "Login: " + user.username);

		var winRate = (user.gamesWon/ (user.gamesLost + user.gamesWon)) * 100;

		if (!winRate) {
			setText(games, "Winrate: " + 'not yet');
		} else {
			setText(games, "Winrate: " + Math.round(winRate, 2) + "%");
		}

		
		setText(longGame, "Longest game: " + (user.longGame || 'not yet'));
		setText(fastGame, "Fastest game: " + (user.fastGame || 'not yet'));
	}

	socket.on('yourUserInfo', function (user) {
		updateFields(user);
	});
}