function Games(window, socket, gamesIds) {
	var document = window.document,
		games = document.getElementById(gamesIds.children.list),
		currentGames = [];

	function reloadUserInfo(games) {
		// To steer clear of any POSSIBLE intersection with myUserInfo
		// in UserInfo module (which is used for a different purpose), we
		// better duplicate the socket event under different name
		socket.emit('selfGameIdentifier', true);
	}

	socket.on('selfGameIdentifier', refreshCurrentGames);

	function ajaxRedirect(game) {
		var hash = game.hash;
		var url = '/game';

		var xhr = new XMLHttpRequest();

		xhr.open('GET', url);

		window.location.href = '#' + hash;

		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				// Simply rewrite document to game.html
				document.documentElement.innerHTML = xhr.responseText;
			}
		};

		xhr.send();
	}

	function refreshCurrentGames(info) {
		var games = info.games,
			user = info.username;
		// Now we separate the flow into two branches
		// 1. If the client is initiator
		// Fallback to http and use AJAX to redirect him straight to
		// game
		// 2. Normal user, then update the list of games
		var i,j,el;

		for (i = 0; i < games.length; i++) {
			if (games[i].initiator === user) {
				ajaxRedirect(games[i]);
				return;
			}
		}

		for (i = 0; i < currentGames.length; i++) {
			for (j = 0; j < games.length; j++) {
				if (currentGames[i].hash === games[j].hash) {
					el = currentGames[i].el;
					currentGames[i] = games[j];
					currentGames[i].el = el;
					el = null;
					currentGames[i].needsUpdate = true;
					games[j].present = true;
				}
			}
		}

		for (i = 0; i < games.length; i++) {
			if (!games[i].present) {
				currentGames.push(games[i]);
			}
		}

		redraw();
	}

	function redraw() {
		/*
			Markup for a game:

					li
						ul
							li
								span.text
									| Id:
								span.Id
									| 12345
							li
								span.text
									| Author: 
								span.Author
									| Player 1
							li
								span.text
									| Players: 
								span.players
									| 2
							li
								button(disabled).hostenBtn
									| Join

		*/
		var i;

		for (i = 0; i < currentGames.length; i++) {
			if (!currentGames[i].el) {
				drawGame(currentGames[i], i);
			} else {
				updateGame(currentGames[i]);
			}

			games.appendChild(currentGames[i].el);
		}
	}

	function updateGame(gameInfo) {
		if (!gameInfo.needsUpdate) {
			return;
		}

		var gameLi = gameInfo.el,
			gameUl = gameLi.childNodes[0],
			numberOfPlayersLi = gameUl.childNodes[2],
			previousText = numberOfPlayersLi.childNodes[1].childNodes[0],
			text;

		text = document.createTextNode(gameInfo.numberOfPlayers);
		previousText.replaceData(0, previousText.length, gameInfo.numberOfPlayers);
	}

	function drawGame(gameInfo, idx) {
		var hash = gameInfo.hash,
			author = gameInfo.initiator,
			numberOfPlayers = gameInfo.numberOfPlayers,
			gameLi, gameUl, hashLi, authorLi, numberOfPlayersLi, buttonLi,
			spanText, span2, button, text, text2;

		gameLi = document.createElement('li');
		gameUl = document.createElement('ul');
		hashLi = document.createElement('li');
		authorLi = document.createElement('li');
		numberOfPlayersLi = document.createElement('li');
		buttonLi = document.createElement('li');

		gameLi.appendChild(gameUl);
		gameUl.appendChild(hashLi);
		gameUl.appendChild(authorLi);
		gameUl.appendChild(numberOfPlayersLi);
		gameUl.appendChild(buttonLi);

		// hashLi proper
		spanText = document.createElement('span');
		spanText.setAttribute('class', 'text');
		text = document.createTextNode('Id: ');
		spanText.appendChild(text);
		span2 = document.createElement('span');
		span2.setAttribute('class', 'Id');
		text2 = document.createTextNode(idx);
		span2.appendChild(text2);
		hashLi.appendChild(spanText);
		hashLi.appendChild(span2);

		// authorLi proper
		spanText = document.createElement('span');
		spanText.setAttribute('class', 'text');
		text = document.createTextNode('Author: ');
		spanText.appendChild(text);
		span2 = document.createElement('span');
		span2.setAttribute('class', 'Author');
		text2 = document.createTextNode(gameInfo.initiator);
		span2.appendChild(text2);
		authorLi.appendChild(spanText);
		authorLi.appendChild(span2);

		// numberOfPlayersLi proper
		spanText = document.createElement('span');
		spanText.setAttribute('class', 'text');
		text = document.createTextNode('Players: ');
		spanText.appendChild(text);
		span2 = document.createElement('span');
		span2.setAttribute('class', 'players');
		text2 = document.createTextNode(gameInfo.numberOfPlayers);
		span2.appendChild(text2);
		numberOfPlayersLi.appendChild(spanText);
		numberOfPlayersLi.appendChild(span2);

		// buttonLi proper
		button = document.createElement('button');
		button.setAttribute('class', 'hostedBtn');
		button.setAttribute('disabled', true);
		text = document.createTextNode('Join');
		button.appendChild(text);
		buttonLi.appendChild(button);

		gameInfo.el = gameLi;
	}

	socket.on('currentGames', reloadUserInfo);

}