function Statistics(window, socket, statisticsIds) {
	// Statistics only logs top-5

	var document = window.document,
		statistics = document.getElementById(statisticsIds.children.list),
		statsLis = statistics.childNodes,
		i;

	function processStatistics(stats) {
		var i, span, text;

		for (i = 0; i < stats.length; i++) {
			span = statsLis[i].childNodes[0];
			text = span.childNodes[0];

			if (!text) {
				text = document.createTextNode('');
				span.appendChild(text);
			}

			text.replaceData(0, text.length, stats[i].username);

			span = statsLis[i].childNodes[1];
			text = span.childNodes[0];
			
			if (!text) {
				text = document.createTextNode('');
				span.appendChild(text);
			}

			text.replaceData(0, text.length, 
				Math.round(stats[i].winrate * 100, 2) + '%');
		}
	}

	socket.on('statistics', processStatistics);
}