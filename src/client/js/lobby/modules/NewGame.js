function NewGame(window, socket, newGameIds) {

	var document = window.document,
		createButton = document.getElementById(newGameIds.children.createGameButton),
		newGameDialog = document.getElementById(newGameIds.children.newGameDialog),
		closeButton = document.getElementById(newGameIds.children.closeNewGameDialogButton),
		numberOfPlayersField = document.getElementById(newGameIds.children.numberOfPlayers),
		startGameButton = document.getElementById(newGameIds.children.startGameButton);

	closeButton.addEventListener('click', function () {
		hideDialog();
	});

	createButton.addEventListener('click', function () {
		showDialog();
	});

	startGameButton.addEventListener('click', function () {
		var numberOfPlayers = numberOfPlayersField.value;

		if (!isNaN(Number(numberOfPlayers)) || !Number(numberOfPlayers)) {
			var newGame = {
				numberOfPlayers: numberOfPlayers
			};

			// Resetting fields
			numberOfPlayersField.value = '';

			// Sending on socket
			socket.emit('newGame', newGame);

			// Next we disable create button
			createButton.setAttribute('disabled', 'true');

			// Next we hide the dialog
			hideDialog();
		}
	});

	function showDialog() {
		newGameDialog.style.display = 'block';
	}

	function hideDialog() {
		newGameDialog.style.display = 'none';
	}
}