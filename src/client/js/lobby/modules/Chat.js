function Chat(window, socket, chatIds) {

	// TODO simplify it ALL
		var document = window.document,
			chatWindow = document.getElementById(chatIds.children.messages),
			messageField = document.getElementById(chatIds.children.input);

		var messageStack = [],
			typingStack = [],
			erasingStack = [];

		var USER_STATES = {
			TYPING: 0,
			ERASING: 1,
			IDLE: 2,
			SENT: 3
		};

		var userState = USER_STATES.IDLE;


		function messagePusher(clazz) {
			return function (msg) {
				messageStack.push( {
					clazz: clazz,
					body: msg,
					element: null
				});

				for (var i = 0; i < typingStack.length; i++) {
					if (typingStack[i].username === msg.username) {
						chatWindow.removeChild(typingStack[i].element);
						typingStack.splice(i, 1);
					}
				}

				for (i = 0; i < erasing.length; i++) {
					if (erasingStack[i].username === msg.username) {
						chatWindow.removeChild(erasingStack[i].element);
						erasingStack.splice(i, 1);
					}
				}

				redraw();
			};
		}

		function statePusher(stateHolder, extractHolder) {
			return function (msg) {
				var flagPresent = false;

				console.log('inside state pusher ' + msg);
				for (var i = 0; i < stateHolder.length; i++) {
					if(stateHolder[i].username === msg) {
						flagPresent = true;
						break;
					}
				}

				for (i = 0; i < extractHolder.length; i++) {
					if(extractHolder[i].username === msg) {
						chatWindow.removeChild(extractHolder[i].element);
						extractHolder.splice(i, 1);
					}
				}

				if (!flagPresent) {
					stateHolder.push({username: msg, element: null});
				}

				redraw();
			};
		}

		function stateExtractor(msg) {

			console.log('inside state extractor ' + msg);
			for (var i = 0; i < typingStack.length; i++) {
				if(typingStack[i].username === msg) {
					chatWindow.removeChild(typingStack[i].element);
					typingStack.splice(i, 1);
					return;
				}
			}

			for (i = 0; i < erasingStack.length; i++) {
				if(erasingStack[i].username === msg) {
					chatWindow.removeChild(erasingStack[i].element);
					erasingStack.splice(i, 1);
					return;
				}
			}
		}

		// Server sent some message...
		socket.on('serverMessage', messagePusher('server'));

		// Someone sent new message...
		socket.on('newMessage', messagePusher('user'));

		// Someone started typing
		socket.on('typing', statePusher(typingStack, erasingStack));

		// Someone is erasing a message ...
		socket.on('erasing', statePusher(erasingStack, typingStack));

		// Someone stopped typing
		socket.on('idle', stateExtractor);

		function redraw() {
			var div, textNode;
			function drawDiv(clazz, text) {
				div = document.createElement('div');
				div.setAttribute('class', clazz);
				textNode = document.createTextNode(text);
				div.appendChild(textNode);	
			}
			for (var i = 0; i < messageStack.length; i++) {
				if (!messageStack[i].element) {
					messageStack[i].element = document.createElement('li');
					drawDiv('chat-name', messageStack[i].body.username);
					messageStack[i].element.appendChild(div);
					drawDiv('chat-message', messageStack[i].body.msg);
					messageStack[i].element.appendChild(div);
				}
				chatWindow.appendChild(messageStack[i].element);
			}

			for (i = 0; i < typingStack.length; i++) {
				if (!typingStack[i].element) {
					typingStack[i].element = document.createElement('li');
					drawDiv('chat-message typing', '# User `' + 
						typingStack[i].username + 
						'` is typing a message');
					typingStack[i].element.appendChild(div);
				}

				chatWindow.appendChild(typingStack[i].element);
			}

			for (i = 0; i < erasingStack.length; i++) {
				if (!erasingStack[i].element) {
					erasingStack[i].element = document.createElement('li');
					drawDiv('chat-message typing', '# User `' + 
						erasingStack[i].username + 
						'` is erasing a message');
					erasingStack[i].element.appendChild(div);
				}

				chatWindow.appendChild(erasingStack[i].element);
			}
		}

		// All socket emit events are attached to the textarea:

		var uMsg = {username : 'user'};

		messageField.addEventListener('keyup', function (evt) {
			if (messageField.value === '' && userState !== USER_STATES.IDLE) {
				userState = USER_STATES.IDLE;
				idle();
			}
		});

		messageField.addEventListener('keydown', function (evt) {
			var keyCode = evt.keyCode;

			// if it is ENTER key...
			// 1. We emit stopped typing
			// 2. We emit stopped erasing
			// 3. If textarea contents aren't empty we emit its contents
			if (keyCode === 13) {
				switch (userState) {
					case USER_STATES.SENT:
						return;
					case USER_STATES.TYPING:
					case USER_STATES.ERASING:
					case USER_STATES.IDLE:
						if (messageField.value !== '') {
							newMessage(messageField.value);
							messageField.value = '';
							userState = USER_STATES.SENT;
							break;
						} else {
							if (userState !== USER_STATES.IDLE) {
								idle();
								userState = USER_STATES.IDLE;	
							}
							break;
						}

					}
					return;
			}

			// if it is backspace key...
			// 1. We emit erasing
			if (keyCode === 8) {
				if (messageField.value === '') {
					// "Slow down, cowboy!"
					return;
				}

				switch(userState) {
					case USER_STATES.ERASING:
					case USER_STATES.SENT:
						break;
					case USER_STATES.IDLE:
					case USER_STATES.TYPING:
						erasing();
						userState = USER_STATES.ERASING;
						break;
				}
				return;
			}

			// if it is typable keyboard key
			// 2. We emit typing
			if ((47 < keyCode && 58 > keyCode) || 
				(64 < keyCode && 91 > keyCode)) {
				switch(userState) {
					case USER_STATES.TYPING:
						break;
					case USER_STATES.IDLE:
					case USER_STATES.ERASING:
					case USER_STATES.SENT:
						typing();
						userState = USER_STATES.TYPING;
						break;
				}
			}
		});

		function newMessage(msg) {
			socket.emit('sendMessage', msg);
		}

		function idle() {
			socket.emit('idle', true);
		}

		function typing() {
			socket.emit('typing', true);
		}

		function erasing() {
			socket.emit('erasing', true);
		}
}