
var TypeUtil = {
	isDefined: function (obj) {
		return typeof(obj) !== 'undefined' && obj !== null;
	},

	isDom: function (obj, morePrecise) {
		try {
			// DOM2-respecting browsers	
			return obj instanceof HTMLElement;
		} catch(e) {
			var olderTagName = obj.olderTagName;
			try {
				obj.tagName = 1;
				obj.tagName = olderTagName;
				return false;
			} catch (e1) {
				return true;
			}
		}
	}
};