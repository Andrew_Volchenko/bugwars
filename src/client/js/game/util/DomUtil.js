var DomUtil = {

	$win: function (el) {
		return this.$doc(el).defaultView || this.$doc(el).parentWindow;
	},

	$doc: function (el) {
		return el.ownerDocument;
	},

	$: function (id) {
		return document.getElementById(id);
	},

	$head: function () {
		return document.getElementsByTagName('head')[0];
	},

	$t: function (text) {
		return document.createTextNode(text);
	},

	$$: function () {
		if (arguments.length > 1) {
				var el = document.createElementNS(arguments[0], arguments[1]);
				return el;
			} else {
				return document.createElement(arguments[0]);
			}
	},

	$a: function (el, attr, val, ns) {
		if (!val) {
			return el.getAttribute(attr);
		}

		if (ns) {
			el.setAttributeNS(ns, attr, val);
		} else {
			el.setAttribute(attr, val);	
		}
	},

	$st: function (el, attr, val) {
		el.style[attr] = val;
	},

	$h: function (el) {
		return parseInt(el.height || 
				el.currentStyle && el.currentStyle.height || 
				!isNaN(parseInt(window.getComputedStyle(el).height, 10)) ? 
				window.getComputedStyle(el).height : 
				el.offsetHeight, 10);
	},

	$w: function (el) {
		return parseInt(el.width || 
				el.currentStyle && el.currentStyle.width || 
				!isNaN(parseInt(window.getComputedStyle(el).width, 10)) ? 
				window.getComputedStyle(el).width : 
				el.offsetWidth, 10);
	},

	$ap: function (el, child) {
		el.appendChild(child);
	},

	$d: function () {
		var from = arguments[0],
			el = arguments[1];

		if (el) {
			from.removeChild(el);
		} else {
			document.documentElement.removeChild(from);	
		}
	},

	$b: function () {
		return document.body;
	}
};