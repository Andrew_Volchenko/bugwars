define(function () {
    var Map = {

        bases : [
            {
                name : "redBaseMain",
                position : {
                    top: "50%",
                    left: "10%"
                }
            },

            {
                name : "blueBaseMain",
                position : {
                    top: "50%",
                    left: "85%"
                }
            },

            {
                name : "neutral1",
                position : {
                    top: "20%",
                    left: "30%"
                }
            },

            {
                name : "neutral2",
                position : {
                    top: "45%",
                    left: "45%"
                }
            },

            {
                name : "neutral3",
                position : {
                    top: "75%",
                    left: "30%"
                }
            },

            {
                name : "neutral4",
                position : {
                    top: "20%",
                    left: "65%"
                }
            },

            {
                name : "neutral5",
                position : {
                    top: "75%",
                    left: "65%"
                }
            }

        ],

        basesCount: 7,

        roads : [
            {start: "redBaseMain", end: "neutral1", len: 3000},
            {start: "redBaseMain", end: "neutral2", len: 5000},
            {start: "redBaseMain", end: "neutral3", len: 3000},
            {start: "blueBaseMain", end: "neutral2", len: 5000},
            {start: "blueBaseMain", end: "neutral4", len: 3000},
            {start: "blueBaseMain", end: "neutral5", len: 3000},
            {start: "neutral2", end: "neutral4", len: 3000},
            {start: "neutral2", end: "neutral5", len: 3000},
            {start: "neutral2", end: "neutral1", len: 3000},
            {start: "neutral2", end: "neutral3", len: 3000}
        ]

    };

    return Map;
});