function WebRTCModule(window, socket, callback) {

	// ICE servers to be used
	var rtcConfiguration = {
		iceServers: [
			{
				urls: 'stun:stun.l.google.com:19302'
			}
		]
	};

	// Configuration for unstable data channel
	var unstableDataChannelConfig = {optional: [{RtpDataChannels: true}]};

	// A shorthand for "peer connection"
	var pc;
	var dataChannel;

	// First, we need to know what game we are in
	// Game is uniquely identified by hash in the url

	var game = window.location.hash;

	// We would need to know whether we have started this game, or not

	var initiator = false;

	// We have no information about TURN servers, we do x XHR to gather info about it
	requestTURNServer(proceed);

	function proceed() {
		// Next we declare we want to either join this game or create this game if it's not there yet
		socket.emit('create or join', game);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////// Sockets

	// If we receive create, we are the initiator

	socket.on('create', function (message) {
		initiator = true;
	});

	// If we receive join, then we can start game, as the game is two-player game 
	// and we are already here and there's the second player joining
	// Note: second player won't receive such notification
	socket.on('join', function (message) {
		// We make sure that the second player has initialized his connection as well
		socket.emit('initializeRTCAndConfirm', game);
	});

	socket.on('initializeRTCAndConfirm', function () {
		function dataChannelAttachedCallback(event) {
			dataChannel = event.channel;
			setUpDataChannelCallbacks();
		}

		if (!initiator) {
			initializePeerConnection();
			pc.ondataChannel = dataChannelAttachedCallback;
			socket.emit('confirmingRTCInitialized', game);
		}
	});

	socket.on('confirmingRTCInitialized', function () {
		if (initiator) {
			initializePeerConnection();
			setUpDataChannel();
			makeOffer();
		}
	});

	socket.on('offer', function (offerDescription) {
		if (!initiator) {
			pc.setRemoteDescription(offerDescription);
			answer();
		}
	});

	socket.on('answer', function (answerDescription) {
		if (initiator) {
			pc.setRemoteDescription(answerDescription);
		}
	});

	//////////////////////////////////////////////////////////////////////////////////////////// WebRTC

	function initializePeerConnection() {
		function handleIceCandidate(event) {
			if (event.candidate) {
				pc.addIceCandidate(event.candidate);
			}
		}

		pc = new webkitRTCPeerConnection(rtcConfiguration, unstableDataChannelConfig);
		pc.onicecandidate = handleIceCandidate;
	}

	function makeOffer() {
		function offerCreatedCallback(offerDescription) {
			pc.setLocalDescription(offerDescription);
			socket.emit('offer', wrapDescription(offerDescription));
		}

		pc.createOffer(offerCreatedCallback);
	}

	function answer() {
		function answerCreatedCallback(answerDescription) {
			pc.setLocalDescription(answerDescription);
			socket.emit('answer', wrapDescription(answerDescription));
		}
		pc.createAnswer(answerCreatedCallback);
	}

	function setUpDataChannel() {
		dataChannel = pc.createDataChannel('dataChannel', {reliable: false});
		setUpDataChannelCallbacks();	
	}

	function setUpDataChannelCallbacks() {
		dataChannel.onmessage = function (event) {
			console.log(event.data);
		};
	}

	//////////////////////////////////////////////////////////////////////////////////////////// TURN request

	function wrapDescription(description) {
		return {game: game, description: description};
	}

	//////////////////////////////////////////////////////////////////////////////////////////// TURN request

	function requestTURNServer(callback) {
		var turnInfoProvider = 'https://computeengineondemand.appspot.com/turn?username=41784574&key=4080218913';

		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4 && xhr.status === 200) {
				var turnServer = JSON.parse(xhr.responseText);
				rtcConfiguration.iceServers.push({
					urls: 'turn:' + turnServer.username + '@' + turnServer.turn,
					credential: turnServer.password
				});

				callback();
			}
		};
	}

}