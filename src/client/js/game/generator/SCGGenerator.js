var SCGGenerator = function () {

	if (arguments.length !== 0) {
		throw new Error ('SCG generator accepts no constructor params!');
	}

	function extend (obj, defaults) {
		if (!obj) {
			return defaults;
		}

		for (var key in defaults) {
			if (!obj[key]) {
				obj[key] = defaults[key];
			}
		}
	}

	function generate(nNodes, cssClasses, opts) {
		var css = extend(cssClasses, {
			red: 'redBaseMain', 
			blue: 'blueBaseMain', 
			neutral: 'base'
		}),
			options = extend(opts, {
				playersPos: 'random', // 'lr', 'tb'
				numberOfCircles: 1,
				central: false,
				randomOffset: true
			}),

			numberOfCircles = options.numberOfCircles,
			central = options.central,
			nodes = [],
			i, j;

			if(central) {
				nNodes--;
				nodes.push({top: '50%', left: '50%'});
			}

			var radius = 40 / numberOfCircles;

			var nodesPerCircle = nNodes / numberOfCircles;

			nodesPerCircle = Math.floor(nodesPerCircle);

			var actualNodesPerCircle = [], wastedNodes = 0;

			for (i = 0; i < numberOfCircles; i++) {
				actualNodesPerCircle[i] = nodesPerCircle;
				wastedNodes += nodesPerCircle;
			}

			var difference = nNodes - wastedNodes;

			i = 0;
			while (difference > 0) {
				actualNodesPerCircle[i++]++;
				difference--;
			}

			actualNodesPerCircle = actualNodesPerCircle.reverse();

			var r, angle, offset = options.randomOffset ? Math.random() * 2 * Math.PI : 0;
			// Starting for the inner circle

			for (i = 0; i < numberOfCircles; i++) {
				r = (i + 1) * radius;
				angle = 2 * Math.PI / actualNodesPerCircle[i];

				for (j = 0; j < actualNodesPerCircle[i]; j++) {
					nodes.push({top: r * Math.sin(offset) + 50, left: r * Math.cos(offset) + 50});
				}
			}

			if (options.playersPos === 'random') {
				var player1 = Math.floor(Math.random() * (++nNodes));
				var player2 = Math.floor(Math.random() * (nNodes));

				nodes[player1].css = css.red;
				nodes[player2].css = css.blue;
			} else {
				var prop = options.playersPos === 'tb' ? 'top' : 'left';
				var minProp = nodes[0], maxProp = nodes[0];

				for (i in nodes) {
					if (nodes[i][prop] < minProp) {
						minProp = nodes[i];
					} else if (nodes[i][prop] > maxProp) {
						maxProp = nodes[i];
					}
				}

				var first = Math.random() > 0.5;

				if (first) {
					minProp[css] = css.red;
					maxProp[css] = css.blue;
				} else {
					minProp[css] = css.blue;
					maxProp[css] = css.red;
				}
			}

			j = 0;

			for (i in nodes) {
				if (!nodes[i].css) {
					nodes[i].css = css.neutral + (++j);
				}
			}

			var json = {bases: [], basesCount: nNodes, roads: []};

			for (i = 0; i < nodes.length; i++) {
				json.bases.push({name: nodes[i].css, position: {top: nodes[i].top, left: nodes[i].left}});

				for (j = i + 1; j < nodes.length; j++) {
					json.roads.push({start: nodes[i].css, end: nodes[j].css});
				}
			}
	}

	return {
		generate: generate
	};
};