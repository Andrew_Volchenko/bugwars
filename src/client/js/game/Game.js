define(['player/Player', 'dom/DOM', 'ui/UI', 'core/Core'], function (player, dom, ui, core) {

    var Player = player;

    var Game = {

		DOM : dom,

		UI : ui,

		Core: core,

        players: []
	};


    // Game.UI.drawmap();
    // Game.Core.spawnPlayers();

    /*
    var player1 = new Player("Steve", "red");
    var player2 = new Player("Bob", "blue");

    Game.players.push(player1);
    Game.players.push(player2);

    player1.startSpawnBugs();
    player2.startSpawnBugs();*/

	return Game;
});