var TypeUtil = require('util/TypeUtil');
var DomUtil = require('util/DomUtil');

var SVGDrawer = function (container) {
	var SVG_SCHEMA = 'http://www.w3.org/2000/svg';

	SVGDrawer.SVG_SCHEMA = SVG_SCHEMA;
	if(!container) {
		throw new Error ('Provide container to SVGDrawer');
	}
	if(!TypeUtil.isDom(container)) {
		throw new Error ('Container must be HtmlElement');
	}

	function adjust(node) {
		var	x1 = parseInt(node.x1, 10) / 100 * $cw() + 25,
			x2 = parseInt(node.x2, 10) / 100 * $cw() + 25,
			y1 = parseInt(node.y1, 10) / 100 * $ch() + 25,
			y2 = parseInt(node.y2, 10) / 100 * $ch() + 25;

		return {x1 : x1, x2: x2, y1: y1, y2: y2};
	}

	function setLineCoordinates(line, node) {
		$a(line, 'x1', node.x1);
		$a(line, 'y1', node.y1);
		$a(line, 'x2', node.x2);
		$a(line, 'y2', node.y2);	
	}

	function defineNodes(nodes) {
		var line, x1, x2, y1, y2, jshint = {};
		for (var idx in nodes) {
			if (!jshint.shutUp) {
				var adjustedNode = adjust(nodes[idx]);
				line = $$(SVG_SCHEMA, 'line');
				$a(line, 'stroke-width', '10');
				$a(line, 'stroke', randomColor());
				$a(line, 'node', JSON.stringify(nodes[idx]));
				$ap(svg, line);

				lines.push(line);
			}
		}

		redraw();
	}

	function adjustNodes() {
		for (var i = 0; i < lines.length; i++) {
			var node = JSON.parse($a(lines[i], 'node'));
			var adjustedNode = adjust(node);
			setLineCoordinates(lines[i], adjustedNode);
		}
	}

	function redraw() {
		$vb(svg, 0, 0, $cw(), $ch());
		adjustNodes();
	}

	function randomColor() {
		function rand() {
			return colorArray[Math.floor(Math.random() * 16)];
		}

		var colorArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
		return '#' + rand() + rand() + rand();
	}
	var svg = $$(SVG_SCHEMA, 'svg'),
		containerWidth, containerHeight,
		observer,
		lines = [];

	container.appendChild(svg);
	$st(svg, 'width', '100%');
	$st(svg, 'height', '100%');
	$vb(svg, 0, 0, $cw(), $ch());

	// Setting mutation observer to readjust viewBox
	function spy(mutationRecords) {
		mutationRecords.forEach(function (record) {
			if (record.attributeName === 'style' ||
				record.attributeName === 'width' ||
				record.attributeName === 'height') {
					redraw();
			}
		});
	}
	if (typeof MutationObserver !== 'undefined') {
		observer = new MutationObserver(spy);
		observer.observe(container, {attributes: true});
	}
	// Next, we set onresize on $win
	var olderOnResize = $win(container).onresize;
	$win(container).onresize = function () {
		if (olderOnResize) {
			olderOnResize.apply(this, arguments);
		}
		redraw();
	};
	return {
		defineNodes: defineNodes
	};
	// Helpers

	function $cw() {
		containerWidth = $w(container);
		return containerWidth;
	}
	function $ch() {
		containerHeight = $h(container);
		return containerHeight;
	}
	function $ap() {
		return DomUtil.$ap.apply(null, arguments);
	}
	function $$() {
		return DomUtil.$$.apply(null, arguments);
	}
	function $doc(el) {
		return DomUtil.$doc(el);
	}
	function $win(el) {
		return DomUtil.$win(el);
	}
	function $st(el, attr, val) {
		return DomUtil.$st(el, attr, val);
	}
	function $w(el) {
		return DomUtil.$w(el);
	}
	function $h(el) {
		return DomUtil.$h(el);
	}
	function $a() {
		return DomUtil.$a.apply(null, arguments);
	}
	function $vb(el, x, y, w, h) {
		$a(el, 'viewBox', x + ' ' + y + ' ' + w + ' ' + h, SVG_SCHEMA);
	}
};
