define(['dom/DOM', 'map/map', 'ui/svg/SVGDrawer'], function (dom, map, SvgDrawer) {
    var UI = {

        drawmap : function(){

            var DOMBase;
            var JSBase;
            var svgDrawer;

            for(var i = 0; i < map.basesCount; i++){
                JSBase = map.bases[i];
                DOMBase = document.createElement("div");
                DOMBase.className = "base";
                DOMBase.setAttribute("id", JSBase.name);
                DOMBase.style.left = JSBase.position.left;
                DOMBase.style.top = JSBase.position.top;
                DOMBase.dataset.bugsCount = 10;
                dom.body.appendChild(DOMBase);
                dom[JSBase.name] = DOMBase;
            }

            svgDrawer = new SvgDrawer(dom.body);
            svgDrawer.defineNodes(this.twoPermutations(map.bases, map.roads));
        },

        twoPermutations: function (points, roads) {

            function isRoad(point1, point2) {

                for (var idx in roads) {
                    if (roads[idx].start === point1.name && roads[idx].end === point2.name ||
                        roads[idx].end === point1.name && roads[idx].start === point2.name) {
                        return true;
                    }
                }

                return false;
            }

            var i, j, permutations = [];

            for (i = 0; i < points.length; i++) {
                for (j = i + 1; j < points.length; j++) {
                    if (i !== j && isRoad(points[i], points[j])) {
                        permutations.push({
                            x1: points[i].position.left, 
                            y1: points[i].position.top, 
                            x2: points[j].position.left, 
                            y2: points[j].position.top
                        });
                    }
                }
            }

            return permutations;
        },

        bugsRoad : {
            firstAnthill    : undefined,
            secondAnthill   : undefined
        },

        currentPlayer: undefined,


        bugsArmy : function(startLoc,endLoc,road,armycount,color) {

            var DOMBug,id,name;
            clearInterval(id);

            DOMBug = document.createElement('div');
            name = 'bugArmy'+dom.bugArmyCount;

            /* Need to add css file for bugs. Only class sets */
            DOMBug.style.position = 'absolute';
            DOMBug.style.height = '30px';
            DOMBug.style.width = '30px';
            DOMBug.style.borderRadius = "50%";
            DOMBug.style.border = "2px solid black";
            /* css file end  */

            DOMBug.style.transition = "all " + map.roads[road].len + "ms linear";
            DOMBug.dataset.bugsCount = armycount;
            DOMBug.setAttribute('id',name);
            DOMBug.style.left = startLoc.style.left;
            DOMBug.style.top = startLoc.style.top;
            DOMBug.style.backgroundColor = color;
            DOMBug.dataset.endPoint = endLoc.id;

            dom.body.appendChild(DOMBug);
            dom[name] = DOMBug;
            dom.bugArmyCount++;

            id = setTimeout(function () {
                DOMBug.style.left = endLoc.style.left;
                DOMBug.style.top = endLoc.style.top;
            }, "2000ms");
        }
    };

    return UI;
});