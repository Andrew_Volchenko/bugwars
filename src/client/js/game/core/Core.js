define(['ui/UI','map/map','dom/DOM','player/Player'],function (ui,map,dom,player) {

	var Player = player;

	var Core = {

		/* customEventListener

			if(Player.isReadyToAttack && Player.haveAccessTo(e.target)){
				fire moveYourBugsEvent
			}

			...
		*/

		/*
		 * Ths time part. It will deleted when upper lvl players will be available
		 */

		players: [],

		spawnPlayers : function(){
			var player1 = new Player("Steve", "red");
			var player2 = new Player("Bob", "blue");

			this.players.push(player1);
			this.players.push(player2);

			player1.startSpawnBugs();
			player2.startSpawnBugs();

			ui.currentPlayer = player2;
		},

		/* End */

        bugsMove : document.body.addEventListener("click", function(e){
			//Shorter name for this SUPER long names
			//Will be added soon
			var first,second,road,bugsCount;

			var access = false;

			if (ui.bugsRoad.firstAnthill !== undefined){
				access = true;
			}
			else {
				access = ui.currentPlayer.haveAccessTo(e.target);
			}

			if (access) {
				if(ui.bugsRoad.firstAnthill === undefined){
					ui.bugsRoad.firstAnthill = e.target;
				} else {
					ui.bugsRoad.secondAnthill = e.target;

					for (var i = 0; i < map.roads.length; i++) {
						if ((map.roads[i].start === ui.bugsRoad.firstAnthill.id &&
							map.roads[i].end === ui.bugsRoad.secondAnthill.id) ||
							(map.roads[i].start === ui.bugsRoad.secondAnthill.id &&
								map.roads[i].end === ui.bugsRoad.firstAnthill.id)) {
							bugsCount = ui.bugsRoad.firstAnthill.dataset.bugsCount;
							ui.currentPlayer.refreshBugsCount(ui.bugsRoad.firstAnthill,0);
							ui.bugsArmy(ui.bugsRoad.firstAnthill,
								ui.bugsRoad.secondAnthill,
								i,
								bugsCount,
								ui.currentPlayer.color);
						}
					}
					ui.bugsRoad.firstAnthill = undefined;
					ui.bugsRoad.secondAnthill = undefined;
				}
			}
			else {
				ui.bugsRoad.firstAnthill = undefined;
			}
		}),

		//Need to add custom events
        armyDisappearance : dom.body.addEventListener('webkitTransitionEnd',function(e){
			var base,rezult;
			base = dom[e.target.dataset.endPoint];
			if(!ui.currentPlayer.haveAccessTo(base)) {
				rezult = base.dataset.bugsCount - e.target.dataset.bugsCount;
			}
			else {
				rezult = parseInt(base.dataset.bugsCount,10) + parseInt(e.target.dataset.bugsCount,10);
			}

			if(rezult < 0){
				ui.currentPlayer.pushBase(base);
				base.dataset.bugsCount = -rezult;
			}
			else {
				base.dataset.bugsCount = rezult;
			}
			dom.body.removeChild(e.target);
			delete dom[e.target.id];
        }, false)
	};

	return Core;
});