define(['dom/DOM'], function (dom) {
    function Player(name,color){

        this.name = name;
        this.color = color;
        this.basesCount = 1;
        this.DOMBases = [dom[this.color + "BaseMain"]];

        this.startSpawnBugs = function(){
            setTimeout(function incBug(_this){
                for(var i = 0; i < _this.basesCount; i++){
                    _this.DOMBases[i].dataset.bugsCount++;
                }
                setTimeout(incBug, 1000, _this);
            }, 1000, this);
        };

        this.haveAccessTo = function(target){
            var flag = false;

            for (var i = 0; i < this.basesCount; i++) {
                if(this.DOMBases[i] === target){
                    flag = true;
                    break;
                }
            }
            return flag;
        };

        this.pushBase = function(target){ 
            for (var i = 0; i < this.basesCount; i++) {
                if(this.DOMBases[i] === target){
                    return;
                }
            }
            this.DOMBases.push(target);
            this.basesCount++;
            target.style.backgroundColor = this.color;
        };

        this.getBugsCount = function(target){
            return target.dataset.bugsCount;
        };

        this.refreshBugsCount = function(target,value){
            target.dataset.bugsCount = value;
        };

        return this;
    }
    return Player;
});