describe('TypeUtil spec includes', function () {
	describe('isDom', function () {
		it('test 1', function () {
			expect(TypeUtil.isDom({})).toBeFalsy();
		});

		it('test 2', function () {
			expect(TypeUtil.isDom(new Image())).toBeTruthy();
		});

		it('test 3, tagName on impostor', function () {
			var impostor = {};
			impostor.tagName = 0;

			expect(TypeUtil.isDom(impostor)).toBeFalsy();
		});

		it('test 4, tagName on read-only prop', function () {
			var impostor = {};
			impostor.tagName = 0;

			Object.defineProperty(impostor, 'tagName', {
				get: function () {},
				set: function () {throw new TypeError();}
			});

			expect(TypeUtil.isDom(impostor)).toBeFalsy();
		});

		describe('test 5, tagName on read-only prop and displaced class', function () {
			var olderHTMLElement = HTMLElement;

			beforeEach(function () {
				olderHTMLElement = HTMLElement;
				HTMLElement = Object;
			});

			afterEach(function () {
				HTMLElement = olderHTMLElement;
			});

			it('', function () {
				var impostor = {};
				impostor.tagName = 0;

				Object.defineProperty(impostor, 'tagName', {
					get: function () {},
					set: function () {throw new TypeError();}
				});

				expect(TypeUtil.isDom(impostor)).toBeTruthy(); // still no way to check against such impostor !
			});
		});
	});
});