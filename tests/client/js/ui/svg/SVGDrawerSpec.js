describe('SVGDrawer specification includes', function () {
	var container, oldRequire;

	beforeEach(function () {
		container = document.createElement('div');
		container.setAttribute('id', 'container');
		container.style.width = '100%';
		container.style.height = '100%';
		container.style.position = 'absolute';
		container.style.top = 0;
		container.style.left = 0;
		document.body.appendChild(container);

		oldRequire = require;

		require = function (def) {
			return def === 'util/TypeUtil' && TypeUtil ||
					def === 'util/DomUtil' && DomUtil
		}
	})

	afterEach(function () {
		// document.body.removeChild(container);
		// container = null;
	});

	it('it is passed a container on init, no container is an error', function () {
		expect(
			function () { 
				var a = new SVGDrawer();
			}).toThrow();
	});

	it('if container is not a DOM node then it is an error too', function () {
		var fakeContainer = {};

		expect(function () {
			var a = new SVGDrawer(fakeContainer);
		}).toThrow();
	});

	it('with real DOM container it is OK', function () {
		new SVGDrawer(container);
	});

	it('has method defineNodes and thats the only one in interface', function () {
		var drawer = new SVGDrawer(container),
			interfacesCount = 0;

		for (var key in drawer) if (drawer.hasOwnProperty(key) && key !== 'auxInterface') {
			interfacesCount++;
			expect(typeof drawer[key]).toBe('function');
			expect(drawer[key].length).toBe(1);
		}

		expect(interfacesCount).toEqual(1);
		console.log(interfacesCount);
	});

	it('in all real it appends svg to container', function () {
		expect(container.childNodes.length).toEqual(0);

		new SVGDrawer(container);

		expect(container.childNodes.length).toEqual(1);
		expect(container.childNodes[0] instanceof SVGSVGElement).toBeTruthy();
	});

	it('appended svg has the same dimensions as container, custom dimensions #1', function () {
		container.style.width = '83%';
		container.style.height = '12.6%';

		new SVGDrawer(container);

		expect(DomUtil.$w(container)).not.toEqual(83);
		expect(DomUtil.$h(container)).not.toEqual(12.6);

		expect(DomUtil.$w(container.childNodes[0])).toEqual(DomUtil.$w(container));
		expect(DomUtil.$h(container.childNodes[0])).toEqual(DomUtil.$h(container));

	});

	it('appended svg has the same dimensions as container, custom dimensions #2', function () {
		container.style.width = '87%';
		container.style.height = '11.3%';

		new SVGDrawer(container);

		expect(DomUtil.$w(container)).not.toEqual(87);
		expect(DomUtil.$h(container)).not.toEqual(11.3);

		expect(DomUtil.$w(container.childNodes[0])).toEqual(DomUtil.$w(container));
		expect(DomUtil.$h(container.childNodes[0])).toEqual(DomUtil.$h(container));
	});

	it('its inner viewport is always of the absolute size of container, #1' , function () {
		new SVGDrawer(container);

		expect(DomUtil.$a(container.childNodes[0], 'viewBox')).toEqual('0 0 ' + 
			DomUtil.$w(container) + ' ' + DomUtil.$h(container));
	});

	it('its inner viewport is always of the absolute size of container, #2', function () {
		container.style.width = '14%';
		container.style.height = '85%';
		new SVGDrawer(container);

		expect(DomUtil.$a(container.childNodes[0], 'viewBox')).toEqual('0 0 ' + 
			DomUtil.$w(container) + ' ' + DomUtil.$h(container));
	});

	it('its inner viewport is always of the absolute size of container, #3', function () {
		container.style.width = '26%';
		container.style.height = '71%';
		new SVGDrawer(container);

		expect(DomUtil.$a(container.childNodes[0], 'viewBox')).toEqual('0 0 ' + 
			DomUtil.$w(container) + ' ' + DomUtil.$h(container));
	});

	it('when the container width changes, svg is resized with it', function () {
		var width = DomUtil.$w(container),
			height = DomUtil.$h(container);

		new SVGDrawer(container);
		
		container.style.width = '100%';
		container.style.height = '51%';

		expect(width).toEqual(DomUtil.$w(container));
		expect(height).not.toEqual(DomUtil.$h(container));

		expect(DomUtil.$w(container.childNodes[0])).toEqual(DomUtil.$w(container));
		expect(DomUtil.$h(container.childNodes[0])).toEqual(DomUtil.$h(container));
	
	});

	it('when the container height changes, svg is resized with it', function () {
		var width = DomUtil.$w(container),
			height = DomUtil.$h(container);

		new SVGDrawer(container);
		
		container.style.width = '51%';
		container.style.height = '100%';

		expect(width).not.toEqual(DomUtil.$w(container));
		expect(height).toEqual(DomUtil.$h(container));

		expect(DomUtil.$w(container.childNodes[0])).toEqual(DomUtil.$w(container));
		expect(DomUtil.$h(container.childNodes[0])).toEqual(DomUtil.$h(container));
	
	});

	it('when the container resizes in both dimensions, svg is resized with it', function () {
		var width = DomUtil.$w(container),
			height = DomUtil.$h(container);

		new SVGDrawer(container);
		
		container.style.width = '51%';
		container.style.height = '49%';

		expect(width).not.toEqual(DomUtil.$w(container));
		expect(height).not.toEqual(DomUtil.$h(container));

		expect(DomUtil.$w(container.childNodes[0])).toEqual(DomUtil.$w(container));
		expect(DomUtil.$h(container.childNodes[0])).toEqual(DomUtil.$h(container));
	
	});

	// Phantomjs doesn't support mutation observers
	if (navigator.userAgent.indexOf('PhantomJS') === -1) {
		it('when the container changes width, svgs internal viewport is readjusted', function () {
			var width = DomUtil.$w(container),
				height = DomUtil.$h(container);

			new SVGDrawer(container);
			
			container.style.width = '51%';
			container.style.height = '100%';

			expect(width).not.toEqual(DomUtil.$w(container));
			expect(height).not.toEqual(DomUtil.$h(container));

			expect(DomUtil.$a(container.childNodes[0], 'viewBox')).toEqual('0 0 ' + 
				DomUtil.$w(container) + ' ' + DomUtil.$h(container));
		});

		it('when the container changes height, svgs internal viewport is readjusted', function () {
			var width = DomUtil.$w(container),
				height = DomUtil.$h(container);

			new SVGDrawer(container);
			
			container.style.width = '100%';
			container.style.height = '51%';

			expect(width).not.toEqual(DomUtil.$w(container));
			expect(height).not.toEqual(DomUtil.$h(container));

			expect(DomUtil.$a(container.childNodes[0], 'viewBox')).toEqual('0 0 ' + 
				DomUtil.$w(container) + ' ' + DomUtil.$h(container));
		});

		it('when the container changes height and width, svgs internal viewport is readjusted', function () {
			var width = DomUtil.$w(container),
				height = DomUtil.$h(container);

			new SVGDrawer(container);
			
			container.style.width = '49%';
			container.style.height = '51%';

			expect(width).not.toEqual(DomUtil.$w(container));
			expect(height).not.toEqual(DomUtil.$h(container));

			expect(DomUtil.$a(container.childNodes[0], 'viewBox')).toEqual('0 0 ' + 
				DomUtil.$w(container) + ' ' + DomUtil.$h(container));
		});
	}

	xdescribe('specification of defineNodes. It is intended to draw lines between pairs of nodes', function () {
		it('', function () {
			var drawer = new SVGDrawer(container);

			drawer.defineNodes([{x1: 0, y1: 0, x2: 0, y2: 0}, {x1: 50, y1: 30, x2: 1, y2: 33}]);

			var svg = container.childNodes[0],
				svgEls = svg.childNodes;

			expect(svgEls.length).toEqual(2);

			var line1 = svgEls[0],
				line2 = svgEls[1];

			expect(line1 instanceof SVGLineElement).toBeTruthy();
			expect(line2 instanceof SVGLineElement).toBeTruthy();

			expect(DomUtil.$a(line1, 'x1')).toBe(null);
			expect(DomUtil.$a(line1, 'y1')).toBe(null);
			expect(DomUtil.$a(line1, 'x2')).toBe(null);
			expect(DomUtil.$a(line1, 'y2')).toBe(null);

			expect(DomUtil.$a(line2, 'x1')).toBe(DomUtil.$w(container) * .5 + '');
			expect(DomUtil.$a(line2, 'y1')).toBe(DomUtil.$h(container) * .3 + '');
			expect(DomUtil.$a(line2, 'x2')).toBe(DomUtil.$w(container) * 0.01 + '');
			expect(DomUtil.$a(line2, 'y2')).toBe(DomUtil.$h(container) * .33 + '');
		});
	})
});