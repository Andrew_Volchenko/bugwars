var page = require('webpage').create();

page.onError = function (error) {
	console.log('error');
	console.log(error);
	phantom.exit();
}

page.onLoadFinished = function (msg) {
	console.log('success');	
	phantom.exit();
}

page.onResourceError = function(resourceError) {
    console.log('Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')');
    console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
};

page.open('placeholder');