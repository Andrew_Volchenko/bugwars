module.exports = (grunt) ->
	grunt.initConfig
		# 1. Clear build directory
		clean:
			build:
				src: 'build'
			# Please refer to the last step
			after:
				files: [
					cwd: 'build'
					src: [
						'client/js/game'
						'**/*.coffee',

					]
					dest: 'build'
					expand: true
				]
		# 2. Copy client/templates, client/js, backend/, server.js to build/..
		copy:
			build:
				cwd: 'src'
				src: ['**', '!**/*.txt']
				dest: 'build'
				expand: true
		# 3. Lint LESS for inconsistencies
		recess:
			build:
				files: [
					cwd: 'build'
					src: ['**/*.less']
					dest: 'build'
					expand: true
				]
				options:
					compile: false # Because compiling LESS is the next step
					noIds: false
					noJsPrefix: false
					noOverqualifying: false
					noUnderscores: false
					noUniversalSelectors: false
					strictPropertyOrder: false
					zeroUnits: false
		# 4. Compile LESS from src/client/less to build/css
		# 5. Minify build/css
		less:
			build:
				files: [
					cwd: 'build'
					src: ['**/*.less']
					dest: 'build'
					expand: true
					ext: '.css'
				]
				options:
					compress: true
					cleancss: true
					ieCompat: true
					strictImports: true
					strictUnits: true
					syncImport: true
					dumpLineNumbers: true
		# 6. Add vendor prefixes to the output css
		autoprefixer:
			build:
				cwd: 'build'
				src: ['**/*.css']
				dest: 'build'
				expand: true
		# 7. Run JSLint/JSHint on build/client/js
		jshint:
			build:
				files: [
					cwd: 'build'
					src: ['**/*.js', '!lib/**/*.js', '!**/less.js']
					dest: 'build'
					expand: true
				]
				options:
					bitwise: true
					camelcase: true
					curly: true
					eqeqeq: true
					es3: true # ÐžÐ±ÑÑƒÐ´Ð¸Ñ‚ÑŒ
					forin: true
					freeze: true
					globals:
						'document': true
						'define': true
						'require': true
						'alert': true
						'setTimeout': true
						'clearTimeout': true
						'setInterval': true
						'clearInterval': true
						'console': true
						'HTMLElement': true
						'window': true
						'MutationObserver': true
						'less': true
						'Mongo': true
						'io': true
						'XMLHttpRequest': true
						'FormData': true
						'webkitRTCPeerConnection': true
					immed: true
					indent: false
					latedef: 'nofunc'
					newcap: true
					noarg: true
					noempty: true
					nonew: true
					plusplus: false # ÐžÐ±ÑÑƒÐ´Ð¸Ñ‚ÑŒ
					undef: true
					unused: false
					strict: false # strict is imposed in wrap.start
					trailing: false
					maxparams: 5
					maxdepth: 4 #ÐžÐ±ÑÑƒÐ´Ð¸Ñ‚ÑŒ
					maxstatements: 150 #ÐžÐ±ÑÑƒÐ´Ð¸Ñ‚ÑŒ
					maxcomplexity: 100
					maxlen: 120
		# 7. Run tests on build/client/js
		jasmine:
			build:
				files: [
					cwd: 'src'
					src: ['client/js/game/**/*.js', '!lib/**/*.js', '!**/less.js']
					dest: 'build'
					expand: true
				]
				options:
					keepRunner: true
					specs: 'tests/**/*Spec.js'
					helpers: 'tests/Mockery.js'
		# 9. Requirefy unrequirefied js files
		requirefy:
			build:
				cwd: 'build'
				src: ['client/js/game/**/*.js', '!client/js/game/webrtc.js']
				dest: 'build'
				expand: true
		# a. Concatenate js files
		requirejs:
			build:
				options:
					name: 'Game'
					baseUrl: 'build/client/js/game'
					out: 'build/client/client.js'
					optimize: 'none'
					wrap:
						startFile: 'wrap/game/wrap.start'
						endFile: 'wrap/game/wrap.end'
		# 8. Compile/minify build/client/js to build/js
		# a. Lint Coffeescript
		coffeelint:
			build:
				files: [
					cwd: ''
					src: ['Gruntfile.coffee' ,'build/**/*.coffee']
				]
				options:
					'arrow_spacing':
						'level': 'error'
					'camel_case_classes':
						'level': 'error'
					'colon_assignment_spacing':
						'level': 'ignore'
					'cyclomatic_complexity':
						'level': 'ignore'
					'duplicate_keys':
						'level': 'error'
					'indentation':
						'level': 'ignore'
					'max_line_length':
						'level': 'error'
					'missing_fat_arrows':
						'level': 'warn'
					'newlines_after_classes':
						'level': 'error'
					'no_backticks':
						'level': 'warn'
					'no_empty_param_list':
						'level': 'error'
					'no_implicit_braces':
						'level': 'ignore'
					'no_implicit_parens':
						'level': 'ignore'
					'no_stand_alone_at':
						'level': 'ignore'
					'no_trailing_whitespace':
						'level': 'ignore'
					'no_tabs':
						'level': 'ignore'
					'no_throwing_strings':
						'level': 'ignore'
					'no_unnecessary_fat_arrows':
						'level': 'error'
					'space_operators':
						'level': 'error'
		# c. Compile CoffeeScript
		coffee:
			build:
				files: [
					cwd: 'build'
					src: ['**/*.coffee']
					dest: 'build'
					expand: true
					ext: '.js'
				]
		# c. Loading files into Phantom js
		phantomTesting:
			build:
				options:
					html: 'build/client/test.html'
					script: 'build/client/client.js'

		# c. Delete LESS, Coffee, non-concatenated JS

	grunt.loadNpmTasks 'grunt-contrib-clean'
	grunt.loadNpmTasks 'grunt-contrib-copy'
	grunt.loadNpmTasks 'grunt-recess'
	grunt.loadNpmTasks 'grunt-contrib-less'
	grunt.loadNpmTasks 'grunt-autoprefixer'
	grunt.loadNpmTasks 'grunt-contrib-jshint'
	grunt.loadNpmTasks 'grunt-contrib-jasmine'
	grunt.loadNpmTasks 'grunt-contrib-requirejs'
	grunt.loadNpmTasks 'grunt-coffeelint'
	grunt.loadNpmTasks 'grunt-contrib-coffee'

	###
		Testing concatenated clien js in phantom js
		If phantom js emits any errors, halt with error
	###
	path = require('path')
	phantom = require('phantomjs').path
	bash = require('child_process').execFile
	grunt.registerMultiTask 'phantomTesting', 
	'Checking html with scripts in phantom js', ->
		# Allowing phantom js to write to grunt console
		
		options = @options {html: '', script: ''}

		html = options.html
		script =
			dir: path.dirname options.script
			name: path.basename options.script

		if html is off or script is off then return

		htmlSource = '<!DOCTYPE html>'
		htmlSource += '<html>'
		htmlSource += '<head>'
		
		requireLine = '<script type="text/javascript" '
		requireLine += 'src="'
		requireLine += '../lib/requirejs/require.js'
		requireLine += '"></script>'

		htmlSource += requireLine

		scriptLine = '<script type="text/javascript" '
		scriptLine += 'src="'
		scriptLine += script.name
		scriptLine += '"></script>'

		scriptLine = scriptLine.replace new RegExp('\\' + path.sep, 'g'), '/'
		htmlSource += scriptLine
		htmlSource += '</head>'
		htmlSource += '<body></body></html>'

		grunt.file.write html, htmlSource

		phantomTesting = 'phantomTesting.js'

		#Now that we have html we have phantomjs script in grunt root
		phantomScript = grunt.file.read phantomTesting
		pathFromPhantomToHtml = path.relative phantom, html
		phantomScript = 
			phantomScript.replace 'page.open(\'placeholder\');', 
			'page.open(\'' + html.replace(new RegExp('\\' + path.sep, 'g'), '/') + '\')'

		pathToScript = 'build/' + phantomTesting
		grunt.file.write(pathToScript, phantomScript)

		# We dont know when phantom gonna exit so we do it async
		done = @async()
		bash phantom, [pathToScript], {}, (err, stdout) ->
			if err is on
				done new Error 'Phantom broke'
				return
			if stdout.substring(0,3) is 'err'
				done new Error('Page ' + html + ' has errors')
				return
			done true
					

	###
		Turns files like this one:

		var ui = require('ui/UI')
		var svgDrawer = require('ui/svg/SVGDrawer')

		var Module = {
			str: 'I am a module',
			str1: function () {
				var a = ui;
				var b = svgDrawer;
			}
		};

		into legal require.js modules
	###
	grunt.registerMultiTask 'requirefy', 'Preparing javascript for require.js', ->
		@files.forEach (file) ->
			sourceCode = grunt.file.read file.src
			if sourceCode.substring(0,6) != 'define'
				console.log file.src
				codeLines = sourceCode.split '\n'
				requires = []
				rest = []
				requires = codeLines.filter (line) ->
					/require.*[^;]/.test line
				requireVars = requires.map (line) ->
					line = (line.match 'var .* =')[0]
					line = line.substring 4, (line.length - 2)
				requires = requires.map (line) ->
					line = (line.match '\'.*\'')[0]
					line.substring 1, (line.length - 1)
				notrequires = codeLines.filter (line) ->
					!(/require.*[^;]/.test line) and line.trim() isnt ''
				moduleName = notrequires.filter (line) ->
					line.trim() isnt ''
				moduleName = moduleName[0]
				moduleName = (moduleName.match 'var .* =')[0]
				moduleName = moduleName.substring(4,(moduleName.length - 2))
				sourceCode = 'define(['
				requires.forEach (req) ->
					sourceCode += '\'' + req + '\','
				if requires.length > 0
					sourceCode = sourceCode.substring 0, (sourceCode.length - 1)
				sourceCode += '], function ('
				requireVars.forEach (req) ->
					sourceCode += req + ','
				if requires.length > 0
					sourceCode = sourceCode.substring 0, (sourceCode.length - 1)
				sourceCode += ') {\n\t' + notrequires.join('\n\t')
				sourceCode += '\n\treturn ' + moduleName + ';\n});\n'
			grunt.file.write file.dest, sourceCode

	# 0. Compiles lobby module
	grunt.registerTask 'compileLobby', '', ->
		chat = 'src/client/js/lobby/modules/Chat.js'
		userInfo = 'src/client/js/lobby/modules/UserInfo.js'
		games  = 'src/client/js/lobby/modules/Games.js'
		statistics = 'src/client/js/lobby/modules/Statistics.js'
		newGame = 'src/client/js/lobby/modules/NewGame.js'

		lobbyIds = 'src/client/js/lobby/lobbyIds.js'

		chatSrc = grunt.file.read chat
		userInfoSrc = grunt.file.read userInfo
		gamesSrc = grunt.file.read games
		statisticsSrc = grunt.file.read statistics
		newGameSrc = grunt.file.read newGame
		lobbyIdsSrc = grunt.file.read lobbyIds

		wrapStart = 'wrap/lobby/wrap.start'
		wrapEnd = 'wrap/lobby/wrap.end'

		wrapStartSrc = grunt.file.read wrapStart
		wrapEndSrc = grunt.file.read wrapEnd

		output = 'build/static/lobby.js'

		outputSrc = lobbyIdsSrc + '\n'
		outputSrc += wrapStartSrc + '\n'
		outputSrc += '(' + chatSrc + '(window, socket, chat));\n'
		outputSrc += '(' + userInfoSrc + '(window, socket, userInfo));\n'
		outputSrc += '(' + gamesSrc + '(window, socket, games));\n'
		outputSrc += '(' + statisticsSrc + '(window, socket, statistics));\n'
		outputSrc += '(' + newGameSrc + '(window, socket, newGame));\n'
		outputSrc += wrapEndSrc

		grunt.file.write output, outputSrc

	# 0. Compile login module
	grunt.registerTask 'compileLogin', '', ->
		loginIds = 'src/client/js/login/loginIds.js'
		registrationForm = 'src/client/js/login/RegistrationForm.js'

		wrapStart = 'wrap/login/wrap.start'
		wrapEnd = 'wrap/login/wrap.end'

		loginIdsSrc = grunt.file.read loginIds
		registrationFormSrc = grunt.file.read registrationForm

		wrapStartSrc = grunt.file.read wrapStart
		wrapEndSrc = grunt.file.read wrapEnd

		output = 'build/static/login.js'

		outputSrc = loginIdsSrc + '\n'
		outputSrc += wrapStartSrc + '\n'
		outputSrc += '(' + registrationFormSrc + 
			'(window, registrationFormIds));' + '\n'
		outputSrc += wrapEndSrc + '\n'

		grunt.file.write output, outputSrc

	grunt.registerTask 'compileGame', '', ->
		wrapStart = 'wrap/game/realWrap.start'
		wrapEnd = 'wrap/game/realWrap.end'

		wrapStartSrc = grunt.file.read wrapStart
		wrapEndSrc = grunt.file.read wrapEnd

		webRTC = 'src/client/js/game/webrtc.js'
		webRTCSrc = grunt.file.read webRTC

		output = 'build/static/game.js'

		outputSrc = wrapStartSrc
		outputSrc += webRTCSrc
		outputSrc += wrapEndSrc

		grunt.file.write output, outputSrc

	# 0. Updater generic task
	update = (updateSystem, cb) ->
        exec = require('child_process').exec
        command = updateSystem + ' install'
        exec command, {cwd: '.'}, (err, stdout, stderr) -> 
            console.log stdout.green
            console.log stderr.red
            cb()

	grunt.registerTask 'npm', ['update/fetch the backend dependencies'], -> 
		update 'npm', @async()
	grunt.registerTask 'bower', 'update/fetch the frontend dependencies', -> 
		update 'bower', @async()
	
	#-0. Include client.js into client.js into game.html
	#		And remove less and replace it with css links.
	grunt.registerTask 'beautifyGameHtml', '', ->
		gameHtml = 'build/static/html/game.html'
		sourceCode = (grunt.file.read gameHtml).split '\n'

		position = 0

		while sourceCode[position].trim() isnt '</head>'
			position++
			if sourceCode[position].indexOf '.less' isnt -1
				sourceCode[position] = sourceCode[position].replace '.less', '.css'

		position--
		sourceCode.splice position, 0, 
		'<script src="../../client/client.js"></script>'
		grunt.file.write gameHtml, sourceCode.join '\n'

	grunt.registerTask 'mongoinstall', 'Run install script for Mongo', ->
		exec = require('child_process').exec
		done = @async()
		mongoscript = 'src/backend/mongo/install.js'
		exec 'mongo ' + mongoscript, (err, stdout) ->
			if err 
				grunt.log.warn 'Couldnt execute mongo from shell to initialize db\n'
				grunt.log.warn 'Original error is: ' + err.message + '\n'
				return
			grunt.log.ok 'Script loaded properly. Use admin@admin to log in into app'
			done()

	grunt.registerTask 'after', ['clean:after', 'beautifyGameHtml']
	grunt.registerTask 'coffeescript', ['coffeelint', 'coffee']
	grunt.registerTask 'javascript', 
	['javascriptTests', 'requirejs', 'compileLogin', 
	'compileLobby', 'compileGame', 'phantomTesting']
	grunt.registerTask 'javascriptTests', ['jshint', 'jasmine', 'requirefy']
	grunt.registerTask 'stylesheets', ['recess','less','autoprefixer']
	grunt.registerTask 'prepareFolder', ['clean:build', 'copy']
	grunt.registerTask 'prelude', ['npm', 'prepareFolder', 'bower']
	grunt.registerTask 'build',
	['mongoinstall', 'prelude','stylesheets', 
	'javascript', 'coffeescript', 'after']
	grunt.registerTask 'default', ['build']